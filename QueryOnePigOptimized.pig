data_rating = LOAD 'hdfs:///source/ratings.csv' USING PigStorage(',') AS (userId:long, movieId:long, rating:double, timestamp:long);
data_movies = LOAD 'hdfs:///source/movies.csv' USING PigStorage(',') AS (movieId:long, title:chararray, genres:chararray);

data_rating_filte = filter data_rating by timestamp>=946684800;

datafinal = join data_rating_filte by movieId, data_movies by movieId;

grpd = group datafinal by data_rating_filte::movieId;
cnt = foreach grpd generate datafinal.data_movies::title, AVG(datafinal.data_rating_filte::rating);

STORE cnt INTO 'hdfs:///QueryOnePIG';




