# PROGETTO 1: MAPREDUCE and HADOOP

## REQUIREMENT
Le query a cui rispondere sono:

  - Individuare i film con una valutazione maggiore o uguale a 4.0 e valutati a partire dal 1 Gennaio 2000.
  - Calcolare la valutazione media e la sua deviazione standard per ciascun genere di film.
  - Trovare i 10 film che hanno ottenuto la piu alta valutazione nell’ultimo anno del dataset (dal 1 Aprile 2014 al 31 Marzo 2015) e confrontare, laddove possibile, la loro posizione nella classifica rispetto a quella conseguita nell’anno precedente (dal 1 Aprile 2013 al 31 Marzo 2014). 

Si chiede inoltre di valutare sperimentalmente i tempi di processamento delle 3 query sulla piattaforma di riferimento usata per la realizzazione del progetto. Tale piattaforma puo essere un nodo standalone oppure in alternativa e possibile utilizzare un servizio Cloud per Hadoop (ad es. Amazon EMR o Google Dataproc), utilizzando i rispettivi grant a disposizione.
Infine, si chiede di realizzare la fase di data ingestion per:
• importare i dati di input in HDFS, eventualmente trasformando la rappresentazione dei dati in un altro
formato (e.g., Avro, Parquet, ...), usando un framework a scelta (e.g., Flume, Kite, ...);
• esportare i dati di output da HDFS ad un sistema di storage a scelta (e.g., HBase, ...)
## NEEDS
Per predisporre l'ambiente di sviluppo abbiamo bisogno dell'istallazione di Hadoop, Flume ed Hbase.
#### Hadoop
      
      In docker l'istallazione di Hadoop è gia fornita tramite la docker image matnar/hadoop

Per quanto riguarda per la versione standalone seguiamo i seguenti passi:
```sh
$ wget http://apache.claz.org/hadoop/common/hadoop-2.4.1/hadoop-2.4.1.tar.gz
$ tar xzf hadoop-2.4.1.tar.gz 
$ mv hadoop-2.4.1/* to hadoop/
```
Dopo aver scaricato il source di Hadoop dobbiamo impostare le variabili di ambiente:

Nel  ~/.bashrc dobbiamo inserire la variabile ambiente:
      
      export HADOOP_HOME=/usr/local/hadoop 

#### Flume
Dopo aver scaricato il bin dal sito di apache Flume procediamo con i seguenti passi: 
```sh
$ cd /usr/local
$ tar zxvf apache-flume-1.6.0-bin.tar.gz
$ mv apache-flume-1.6.0-bin/* to HBASE/
```
Aggiungere le seguenti variabili ambienti :
            
      export FLUME_HOME=/home/user/apache-flume-1.6.0-bin/
      export PATH=$PATH:$FLUME_HOME/bin/

Ed importiamo il path di Java nel flume-env.sh
            
      export JAVA_HOME = /your/java/path

Dopo di che andiamo a modificare il file di configurazione per un agente flume.
Nel nostro caso abbiamo creato creato due agent ed adoperato come source una spool directory, dove un agent si occupa della directory dei movies ed uno dei rating. Possiamo vedere il file di configurazione nel repository FlumeConf.conf

#### Hbase
Dopo aver scaricato il bin dal sito di apache HBase procediamo con i seguenti passi: 
```sh
$ wget http://www.interior-dsgn.com/apache/hbase/stable/hbase-x.x.x-hadoop2-bin.tar.gz
$ tar -zxvf hbase-x.x.x-hadoop2-bin.tar.gz
```
Dopo aver scompattato il file, andiamo a modificare hbase-env.sh come segue:
      
      export JAVA_HOME=/usr/lib/jvm/java-1.7.0

e il file hbase-site.xml:

```xml
<configuration>
   //Here you have to set the path where you want HBase to store its files.
   <property>
      <name>hbase.rootdir</name>
      <value>file:/home/hadoop/HBase/HFiles</value>
   </property>
	//Here you have to set the path where you want HBase to store its built in zookeeper  files.
   <property>
        <name>hbase.zookeeper.property.dataDir</name>
     <value>/home/hadoop/zookeeper</value>
   </property>
</configuration>
```


In conclusione facciamo l'aggiornamento del ~/.bashrc con:
```sh
$ source ~/.bashrc
```
## START UP
Seguiamo i seguenti passi per lanciare l'applicazione per eseguire le query mapReduce.

- Lanciamo come primo elemento HADOOP con Hadoop Distribuited File System(HDFS):
```sh
$ start-all.sh
```
- Lanciamo per secondo elemento lo starting di Hbase:
```sh
$ start-hbase.sh
```
- Lanciamo come terzo passo il client Hbase che si occupa di creare le tabelle per salvare i risultati:

```sh
$ hadoop jar hbase-client.jar HbaseClient 
```
- Come quarto step lanciamo gli agent di Flume per effettuare injection dei dati, nel caso contrario possiamo utilizzare hdfs dfs -put localfile hdfs:///directory. 
  Per utilizzare flume ci creiamo prima le directory che fungono da sink
  
```sh
$ hdfs dfs -mkdire hdfs:///flumesink
$ flume-ng agent -n agent1 -f /usr/lib/flume/conf/flumeConf.conf
$ flume-ng agent -n agent2 -f /usr/lib/flume/conf/flumeConf.conf
```
- Finito l'injection dei dati come ultimo step lanciamo i comandi da eseguire su hadoop per quanto riguarda le query il quale ci permette di scegliere volta in volta le varie query e di inizializzare alcuni paramentri come il numero di reduce ed il numero di esecuzioni.
```sh
$ hadoop jar SABDMapReduce SABD_Project
```
#### ON AIR

Una volta lanciato il codice che esegue le query, possiamo scegliere ed associare diversi valori:
     
      - La directory dei file di input
      
      - La directory dei file di output
      
      - La query da eseguire
      
      - Il numero di test per ogni Query
      
      - Il numero di reduce per le Query