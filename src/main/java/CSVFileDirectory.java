import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * Create the set of results directories.
 * Use the Singleton Pattern.
 *  where one row is matched to one directory.
 *
 * @author  Davide Nanni
 * @version %I%, %G%
 *
 */

public class CSVFileDirectory
{
    private static CSVFileDirectory dir = null;
    private static boolean FirstRun = true;


    private CSVFileDirectory(){};

    /**
     * Create the results directory.
     *
     * @param pathname
     * @return
     */
    public static CSVFileDirectory setUpResultDirectories(String pathname)
    {
        if ( dir == null)
        {
            createDirectory(pathname);
            
            System.out.println("[CREATED ALL RESULT DIRECTORIES]");

            dir = new CSVFileDirectory();
        }
        else
        {
            System.out.println("[ERROR] Result Directories NOT Created!");
        }

        return dir;
    }


    /**
     * Check if there is a directory with the same name.
     *      In this case erases its contents, otherwise creates a new directory.
     *
     * @param path_dir
     * @return
     */
    private static void createDirectory(String path_dir)
    {
        File directory = new File(path_dir);

        if ( !directory.exists())
        {
            if ( !directory.mkdirs() )
            {
                System.out.println("[ERROR] The Results directory is not created!");
                System.exit(1);
            }
            else
            {
                directory.setWritable(true,true);
            }
        }
        else
        {
            for (String file : directory.list())
            {
                (new File(path_dir+"/"+file)).delete();
            }
        }

        System.out.println("[CREATED RESULT DIRECTORY] "+path_dir);
    }
}
