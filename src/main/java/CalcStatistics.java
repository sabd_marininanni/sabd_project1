import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by root on 11/06/17.
 */
public class CalcStatistics
{
    private static final String RESULT_DIRECTORY = "/data/ExperimentsResults";

    public static void main(String args[]) throws IOException
    {
        calcAvgStd();
    }

    /**
     *  Calc Average and the Standard Deviation of Times Execution for all queries
     *          stored into RESULT_DIRECTORY
     *
     * @throws IOException
     */
    private static void calcAvgStd() throws IOException
    {
        File directory = new File(RESULT_DIRECTORY);

        CSVFileBasic fileResults = new CSVFileBasic("avgStatistics", RESULT_DIRECTORY);

        String[] header = {"query","avg","std"};

        fileResults.insert_header(header);

        double[] stats = { 0.0, 0.0 };

        String[] listFiles = directory.list();

        for(String file : listFiles)
        {
            if ( !(file.equals("avgStatistics.csv")))
            {
                stats = calcStatistics(file);

                fileResults.insert_value(file.substring(0,file.indexOf(".")));
                fileResults.insert_comma_delimiter();
                fileResults.insert_double_values_one_row(stats);
            }
        }

        fileResults.close();
    }

    /**
     *   Calc the average time execution with its relative standard deviation
     *          for the single query
     *
     * @param file
     * @return double[]
     * @throws IOException
     */
    private static double[] calcStatistics(String file) throws IOException
    {
        BufferedReader f = new BufferedReader( new FileReader(RESULT_DIRECTORY +"/"+file));

        f.readLine();

        String row = "";

        double[] stats = { 0.0, 0.0 };

        double dist = 0.0,
                var = 0.0;

        int i = 1;

        while( (row = f.readLine()) != null)
        {
            double timeValue = Double.parseDouble((row.split(","))[1]);

                    /*
                     *  Welford's Algorithm
                     */
            dist = timeValue - stats[0];

            var += (dist*dist * (i-1))/i;


                    /*
                     * average time
                     */
            stats[0] += dist / i;

                    /*
                     * standard deviation
                     */
            stats[1] = Math.sqrt(var/i);

            ++i;
        }

        f.close();

        return stats;
    }

}
