/**
 * Created by GM on 24/05/2017.
 */

import com.google.gson.Gson;
import com.google.protobuf.ServiceException;
import designpattern.hierarchical.TopicItemsHierarchy;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;


import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class QueryOneHBase {

    private static String HBASE_QUERYONE = "queryOne";

    private static byte[] b(String s){
        return Bytes.toBytes(s);
    }

    public static long getTimeStamp(String dateString) throws ParseException
    {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = dateFormat.parse(dateString);
        long unixTime = (long) date.getTime()/1000;

        return unixTime;
    }

    public static abstract class GenericHierarchyMapper extends Mapper<Object, Text, Text, Text> {

        private Text outKey = new Text();
        private Text outValue = new Text();
        private final String valuePrefix;
        private long thresholdTimestamp = getTimeStamp("01-01-2000");


        protected GenericHierarchyMapper(String valuePrefix) throws ParseException {
            this.valuePrefix = valuePrefix;
        }

        @Override
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

          String line = value.toString();
          if(valuePrefix.equals("R")){
            String[] parts = line.split(",");
              if (parts.length != 4)
                  return;
              if(!(parts[3]).equals("timestamp")){
                if (Long.parseLong(parts[3]) >= thresholdTimestamp) {
                    //System.out.println("pars[3]" + parts[3]);
                    String id = parts[1];
                    String content = parts[2];

                    outKey.set(id);
                    outValue.set(valuePrefix + content);
                    context.write(outKey, outValue);
                }
              }
            }else{
              String[] parts = line.split("," , 2);
              if(parts.length !=2)
                return;
              int i  = (parts[1]).lastIndexOf(",");
              if(!parts[0].equals("movieId")){
                String id =parts[0];
                String content = (parts[1]).substring(0,i);
                outKey.set(id);
                outValue.set(valuePrefix + content);
                context.write(outKey,outValue);
              }
            }
        }
    }

    public static class RatingMapper extends GenericHierarchyMapper {
        public RatingMapper() throws ParseException {
            super("R");
        }
    }

    public static class MoviesMapper extends GenericHierarchyMapper {
        public MoviesMapper() throws ParseException {
            super("M");
        }
    }

    public static class TopicHierarchyReducer extends
            TableReducer<Text, Text, ImmutableBytesWritable>{


        public enum ValueType {RATING, MOVIES, UNKNOWN}
        private Gson gson = new Gson();

        public void reduce(Text key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {
            double res = 0.0;
            double sum = 0.0;
            double n_rep = 0.0;
            String s = " ";

            String tit = " ";

            Text title = new Text();
            for (Text t : values) {
                String value = t.toString();
                if (ValueType.RATING.equals(discriminate(value))) {
                    s = value.substring(1,value.length());
                    double add = Double.parseDouble(s);
                    sum = sum + add;
                    n_rep = n_rep + 1;
                } else if (ValueType.MOVIES.equals(discriminate(value))){
                    tit = value.substring(1,value.length());
                }
            }
            title.set(tit);
            res = sum / n_rep;

            if (res >= 4.0) {
                /* Serialize topic */
                String serializedTopic = gson.toJson(res);

                Put p = new Put(b("row"+i));

                ++i;

                p.add(b("fam1"),b("titleFilm"),b(tit));
                p.add(b("fam1"),b("avgRating"),b(""+res));

                context.write(null, p);
            }
        }

        private ValueType discriminate(String value){

            char d = value.charAt(0);
            switch (d){
                case 'R':
                    return ValueType.RATING;
                case 'M':
                    return ValueType.MOVIES;
            }

            return ValueType.UNKNOWN;
        }

        private String getContent(String value){
            return value.substring(1);
        }

    }

    private static int i = 0;


    public static double[] runTest(String... args) throws IOException, ClassNotFoundException, InterruptedException, ServiceException
    {
        Configuration conf = HBaseConfiguration.create();

                    /* Check configuration */
        HBaseAdmin.checkHBaseAvailable(conf);

        HBaseAdmin admin = new HBaseAdmin(conf);

                    /* Init 'queryOne' HBase Table */
        if ( admin.tableExists(HBASE_QUERYONE))
        {
            admin.disableTable(HBASE_QUERYONE);
            admin.deleteTable(HBASE_QUERYONE);

            HTableDescriptor tableDescriptor = new HTableDescriptor(TableName.valueOf(HBASE_QUERYONE));
            tableDescriptor.addFamily(new HColumnDescriptor("fam1"));
            admin.createTable(tableDescriptor);
        }

                    /* Job */
        Job job = Job.getInstance(conf, "QueryOneHBase");

        job.setJarByClass(QueryOneHBase.class);

        /* Map function, from multiple input file */
        MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, RatingMapper.class);
        MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, MoviesMapper.class);

        /* Reduce function */
        TableMapReduceUtil.addDependencyJars(job);
        TableMapReduceUtil.initTableReducerJob(HBASE_QUERYONE, TopicHierarchyReducer.class, job );
        job.setReducerClass(TopicHierarchyReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        /* Set output files/directories using command line arguments */

        /* Wait for job termination */
        long start = new Date().getTime();
        int code = job.waitForCompletion(true) ? 0 : 1;
        long end = new Date().getTime();


            /*
             *  Calc Execution Time single step.
             */
        double[] executionTime = {(end-start)/1000};

        return executionTime;
    }
}
