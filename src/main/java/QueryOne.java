/**
 * Created by GM on 24/05/2017.
 */
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.io.DoubleWritable;
import com.google.gson.Gson;
import designpattern.hierarchical.TopicItemsHierarchy;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import util.Topic;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
public class QueryOne {

    public static long getTimeStamp(String dateString) throws ParseException
    {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = dateFormat.parse(dateString);
        long unixTime = (long) date.getTime()/1000;

        return unixTime;
    }

    public static abstract class GenericHierarchyMapper extends Mapper<Object, Text, Text, Text> {

        private Text outKey = new Text();
        private Text outValue = new Text();
        private final String valuePrefix;
        private long thresholdTimestamp = getTimeStamp("01-01-2000");


        protected GenericHierarchyMapper(String valuePrefix) throws ParseException {
            this.valuePrefix = valuePrefix;
        }

                /**
                  * This map uses the pattern hierarchy with two files, rating and movies.
                  * into the tuble of rating type maps the <K,V> as  <movieId,rating> while for the string of
                  * type movies maps the <K,V> as <movieID,nameFilm>
                  * @param key
                  * @param value
                  * @param context
                  * @throws IOException
                  * @throws InterruptedException
                 */
        @Override
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

          String line = value.toString();
          if(valuePrefix.equals("R")){
            String[] parts = line.split(",");
              if (parts.length != 4)
                  return;
              if(!(parts[3]).equals("timestamp")){
                if (Long.parseLong(parts[3]) >= thresholdTimestamp) {
                    String id = parts[1];
                    String content = parts[2];

                    outKey.set(id);
                    outValue.set(valuePrefix + content);
                    context.write(outKey, outValue);
                }
              }
            }else{
              String[] parts = line.split("," , 2);
              if(parts.length !=2)
                return;
              int i  = (parts[1]).lastIndexOf(",");
              if(!parts[0].equals("movieId")){
                String id =parts[0];
                String content = (parts[1]).substring(0,i);
                outKey.set(id);
                outValue.set(valuePrefix + content);
                context.write(outKey,outValue);
              }
            }
        }
    }

    public static class RatingMapper extends GenericHierarchyMapper {
        public RatingMapper() throws ParseException {
            super("R");
        }
    }

    public static class MoviesMapper extends GenericHierarchyMapper {
        public MoviesMapper() throws ParseException {
            super("M");
        }
    }

    public static class TopicHierarchyReducer extends
            Reducer<Text, Text, Text, Text> {

        public enum ValueType {RATING, MOVIES, UNKNOWN}
        private Gson gson = new Gson();


               /**
                  * this Reduce for the tuple of each Key, if the tuple is rating type sum the rating and
                  * count the num of the vote, while
                  * if it is of movies type memorize the name of the film.
                  * The output of the Task is <Movie name, avg Rating>
                  * @param key
                  * @param values
                  * @param context
                  * @throws IOException
                  * @throws InterruptedException
                  */
        @Override
        public void reduce(Text key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {
            double res = 0.0;
            double sum = 0.0;
            double n_rep = 0.0;
            String s = " ";
            String tit = " ";
            Text title = new Text();
            for (Text t : values) {
                String value = t.toString();
                if (ValueType.RATING.equals(discriminate(value))) {
                    s = value.substring(1,value.length());
                    double add = Double.parseDouble(s);
                    sum = sum + add;
                    n_rep = n_rep + 1;
                } else if (ValueType.MOVIES.equals(discriminate(value))){
                    tit = value.substring(1,value.length());
                }
            }
            title.set(tit);
            res = sum / n_rep;
            if (res >= 4.0) {
                /* Serialize topic */
                String serializedTopic = gson.toJson(res);
                context.write(title, new Text(serializedTopic));
            }
        }

        private ValueType discriminate(String value){

            char d = value.charAt(0);
            switch (d){
                case 'R':
                    return ValueType.RATING;
                case 'M':
                    return ValueType.MOVIES;
            }

            return ValueType.UNKNOWN;
        }

        private String getContent(String value){
            return value.substring(1);
        }

    }


    public static double[] runTest(String... args) throws IOException, ClassNotFoundException, InterruptedException
    {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "TopicHierarchy");
        job.setJarByClass(TopicItemsHierarchy.class);


        /* Map function, from multiple input file */
        MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, RatingMapper.class);
        MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, MoviesMapper.class);

        /* Reduce function */
        job.setReducerClass(TopicHierarchyReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

            /* Set output files/directories using command line arguments */
        if ( Integer.parseInt(args[3]) == 0)
        {
            args[2] = args[2].substring(0,args[2].length()-1);
        }


        FileOutputFormat.setOutputPath(job, new Path(args[2]));
        job.setOutputFormatClass(TextOutputFormat.class);

        /* Wait for job termination */

        long start = new Date().getTime();
        int code = job.waitForCompletion(true) ? 0 : 1;
        long end = new Date().getTime();


                    /*
                     *  Delete Multiple Data generated into a multi run testing session
                     */
        if ( Integer.parseInt(args[3]) != 0)
        {
            FileSystem hdfs = FileSystem.get(conf);
            hdfs.delete(new Path(args[2]),true);
        }

            /*
             *  Calc Execution Time single step.
             */
        double[] executionTime = {(end-start)/1000};

        return executionTime;
    }
}
