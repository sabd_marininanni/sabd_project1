import com.google.protobuf.ServiceException;

import java.io.*;


/**
 * Created by root on 07/06/17.
 */
public class SABD_Project
{
    private static final String RESULT_DIRECTORY = "/data/ExperimentsResults";


    public static void main(String args[]) throws IOException, ClassNotFoundException, InterruptedException, ServiceException
    {
        boolean endTest = false;

        String[] dataset = {"",""};

        int numRun = 0;

            /* Create HBase Tables */

        CSVFileDirectory dir = CSVFileDirectory.setUpResultDirectories(RESULT_DIRECTORY);

            /* Choose the Dataset */

        System.out.println("\n[DATASET]");

        dataset = chooseDataset();

        numRun = getIntegerInputUser("Insert Session Num Run Test (>0): ");

        System.out.println("\n[TEST]");

        do
        {
            singleTest(numRun, dataset);

            endTest = exitApp();
        }
        while(!endTest);

		System.exit(0);
    }

    /**
     *  Exit by App
     *
     * @return
     * @throws IOException
     */
    private static boolean exitApp() throws IOException {
        String exitApp = "";

        boolean wrongInput = false;

        do
        {
            wrongInput = false;

            exitApp = getInputUser("Another Test? (y/n): ");

            if (!(  exitApp.charAt(0)=='y' ||
                    exitApp.charAt(0)=='Y' ||
                    exitApp.charAt(0)=='N' ||
                    exitApp.charAt(0)=='n'))
            {
                wrongInput = true;
            }
        }
        while(wrongInput);

        if ((exitApp.charAt(0)=='y' || exitApp.charAt(0)=='Y'))
        {
            return false;
        }
        else
            return true;
    }

    /**
     * Return the String value input by Standard Input
     *
     * @param msg
     * @return
     * @throws IOException
     */
    private static String getInputUser(String msg) throws IOException
    {
        BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("\n "+msg);

        return buff.readLine();
    }


    /**
     * Return the integer value input by Standard Input
     *
     * @param msg
     * @return
     * @throws IOException
     */
    private static int getIntegerInputUser(String msg) throws  IOException
    {
        return Integer.parseInt(getInputUser(msg));
    }


    /**
     *  User chooses the input dataset
     *
     * @return
     * @throws IOException
     */
    public static String[] chooseDataset() throws IOException
    {

        String[] dataset = {"",""};

        dataset[0] = getInputUser("Insert HDFS Path Rating.csv file: ");

        dataset[1] = getInputUser( "Insert HDFS Path Movies.csv file: ");

        return dataset;
    }


    /**
     *  Run the Session Test
     *
     *
     * @param numRun
     * @param dataset
     * @throws IOException
     * @throws InterruptedException
     * @throws ServiceException
     * @throws ClassNotFoundException
     */
    public static void singleTest(int numRun, String... dataset) throws IOException, InterruptedException, ServiceException, ClassNotFoundException {
        int test = 0;

        test = chooseTest();

        runTest(test, numRun, dataset);
    }


    /**
     *  User chooses a Test from the List
     *
     * @return
     * @throws IOException
     */
    public static int chooseTest() throws IOException
    {
        System.out.println("1) QueryOne (HDFS)");
        System.out.println("2) QueryOne (HBASE)");
        System.out.println("3) QueryTwo (HDFS)");
        System.out.println("4) QueryTwo (HBASE)");
        System.out.println("5) QueryThree (HDFS)");
        System.out.println("6) QueryThree (HBASE)");

        return getIntegerInputUser("Choose a Test (1-6): ");
    }


    /**
     *
     * Run the previously choosen test
     *
     *
     * @param choosenTest
     * @param numRun
     * @param dataset
     * @throws InterruptedException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws ServiceException
     */
    public static void runTest(int choosenTest, int numRun, String... dataset) throws InterruptedException, IOException, ClassNotFoundException, ServiceException
    {
        switch(choosenTest)
        {
            case 1:
                    queryOneHDFS( numRun, dataset ); break;

            case 2:

                    queryOneHBase( numRun, dataset ); break;

            case 3:

                    queryTwoHDFS( numRun, dataset ); break;

            case 4:

                    queryTwoHBase( numRun, dataset ); break;

            case 5:

                    queryThreeHDFS( numRun, dataset ); break;

            case 6:
                    queryThreeHBase( numRun, dataset ); break;
        }
    }

    /**
     *  Execute the Query One - Save the results output into a HDFS File
     *
     * @param numRun
     * @param dataset
     * @throws IOException
     * @throws InterruptedException
     * @throws ClassNotFoundException
     * @throws ServiceException
     */
    private static void queryOneHDFS( int numRun, String... dataset ) throws IOException, InterruptedException, ClassNotFoundException {

        int i = 0;

        double[] resultsExperiments;

        String pathOutput = getInputUser("Insert HDFS Path Output Directory: ");

        CSVFileBasic file = new CSVFileBasic("queryOne","/data/ExperimentsResults/");
        String[] header = {"run","time"};
        file.insert_header(header);

        for( ; i<numRun; ++i )
        {
            resultsExperiments = QueryOne.runTest(dataset[0], dataset[1], pathOutput+i, (""+i));

            file.insert_double_values_one_row_with_index(i,resultsExperiments);
        }

        file.close();
    }

    /**
     *  Execute the Query One - Save the results output into a HBase Table (queryOne)
     *
     * @param numRun
     * @param dataset
     * @throws IOException
     * @throws InterruptedException
     * @throws ClassNotFoundException
     * @throws ServiceException
     */
    private static void queryOneHBase( int numRun, String... dataset ) throws IOException, InterruptedException, ServiceException, ClassNotFoundException {

        int i = 0;

        double[] resultsExperiments;

        CSVFileBasic file1HB = new CSVFileBasic("queryOneHBase", RESULT_DIRECTORY);
        String[] header = {"run","time"};
        file1HB.insert_header(header);

        for( ; i<numRun; ++i )
        {
            resultsExperiments = QueryOneHBase.runTest(dataset[0], dataset[1]);

            file1HB.insert_double_values_one_row_with_index(i,resultsExperiments);
        }

        file1HB.close();
    }


    private static int getNumReducer(String msg) throws IOException
    {
        int numReducer = 0;

        do
        {
            numReducer = getIntegerInputUser(msg);
        }
        while(numReducer <= 0);

        return numReducer;
    }

    /**
     *  Execute the Query Two - Save the results output into a HDFS File
     *
     * @param numRun
     * @param dataset
     * @throws IOException
     * @throws InterruptedException
     * @throws ClassNotFoundException
     * @throws ServiceException
     */
    private static void queryTwoHDFS( int numRun, String... dataset ) throws IOException, InterruptedException, ClassNotFoundException {


        int i = 0;

        double[] resultsExperiments;

        String  pathOutput1 = getInputUser(" Insert HDFS Path Output1 Directory: "),
                pathOutput2 = getInputUser(" Insert HDFS Path Output2 Directory: ");

        int numReducer = getNumReducer("Insert Numb Reducer Step(>0): ");

        CSVFileBasic file2HDFS = new CSVFileBasic("queryTwo", RESULT_DIRECTORY);

        String[] header = {"run","time","firstStep","secondStep"};

        file2HDFS.insert_header(header);

        for( ; i <numRun; ++i )
        {
            resultsExperiments = QueryTwo2.runTest(numReducer, dataset[0], dataset[1], pathOutput1+i,pathOutput2+i,(""+i));

            file2HDFS.insert_double_values_one_row_with_index(i,resultsExperiments);
        }

        file2HDFS.close();
    }


    /**
     *  Execute the Query Two - Save the results output into a HBase Table (queryTwo)
     *
     * @param numRun
     * @param dataset
     * @throws IOException
     * @throws InterruptedException
     * @throws ClassNotFoundException
     * @throws ServiceException
     */
    private static void queryTwoHBase( int numRun, String... dataset ) throws IOException, InterruptedException, ClassNotFoundException, ServiceException {


        int i = 0;

        double[] resultsExperiments;

        String pathOutput = getInputUser(" Insert HDFS Path Output Directory: ");

        int numReducer = getNumReducer("Insert Numb Reducer First Step(>0): ");

        CSVFileBasic file2HBase = new CSVFileBasic("queryTwoHBase", RESULT_DIRECTORY);

        String[] header = {"run","time","firstStep","secondStep"};

        file2HBase.insert_header(header);

        for( ; i <numRun; ++i )
        {
            resultsExperiments = QueryTwoHBase.runTest( numReducer,
                                                    dataset[0],
                                                        dataset[1],
                                                        pathOutput+i,
                                                        (""+i));

            file2HBase.insert_double_values_one_row_with_index(i,resultsExperiments);
        }

        file2HBase.close();
    }


    /**
     *  Execute the Query Three - Save the results output into a HDFS File
     *
     * @param numRun
     * @param dataset
     * @throws IOException
     * @throws InterruptedException
     * @throws ClassNotFoundException
     * @throws ServiceException
     */
    private static void queryThreeHDFS( int numRun, String... dataset ) throws IOException, InterruptedException, ClassNotFoundException, ServiceException {


        int i = 0;

        double[] resultsExperiments;

        String  pathOutput = getInputUser(" Insert HDFS Path Output1 Directory: "),
                pathOutput2 = getInputUser(" Insert HDFS Path Output2 Directory: "),
                pathOutput3 = getInputUser(" Insert HDFS Path Output3 Directory: "),
                pathOutput4 = getInputUser(" Insert HDFS Path Output4 Directory: ");

        int     numReducer = getNumReducer("Insert Numb Reducer First Step(>0): "),
                numReducer2 = getNumReducer("Insert Numb Reducer Second Step(>0): ");

        CSVFileBasic file3HDFS = new CSVFileBasic("queryThree", RESULT_DIRECTORY);

        String[] header = {"run","time","firstStep","secondStep","thirdStep", "fourthStep"};

        file3HDFS.insert_header(header);

        for( ; i <numRun; ++i )
        {
            resultsExperiments = QueryThree.runTest(numReducer,
                                                numReducer2,
                                            dataset[0],
                                                 dataset[1],
                                                pathOutput+i,
                                                pathOutput2+i,
                                                pathOutput3+i,
                                                pathOutput4+i,
                                                (""+i));

            file3HDFS.insert_double_values_one_row_with_index(i,resultsExperiments);
        }

        file3HDFS.close();
    }


    /**
     *  Execute the Query Three - Save the results output into a HBase Table (queryThree)
     *
     * @param numRun
     * @param dataset
     * @throws IOException
     * @throws InterruptedException
     * @throws ClassNotFoundException
     * @throws ServiceException
     */
    private static void queryThreeHBase( int numRun, String... dataset ) throws IOException, InterruptedException, ClassNotFoundException, ServiceException {


        int i = 0;

        double[] resultsExperiments;

        String  pathOutput = getInputUser(" Insert HDFS Path Output1 Directory: "),
                pathOutput2 = getInputUser(" Insert HDFS Path Output2 Directory: "),
                pathOutput3 = getInputUser(" Insert HDFS Path Output3 Directory: ");

        int     numReducer = getNumReducer("Insert Numb Reducer First Step(>0): "),
                numReducer2 = getNumReducer("Insert Numb Reducer Second Step(>0): ");

        CSVFileBasic file3HBase = new CSVFileBasic("queryThreeHBase", RESULT_DIRECTORY);

        String[] header = {"run","time","firstStep","secondStep","thirdStep","fourthStep"};

        file3HBase.insert_header(header);

        for( ; i <numRun; ++i )
        {
            resultsExperiments = QueryThreeHBase.runTest(numReducer,
                    numReducer2,
                    dataset[0],
                    dataset[1],
                    pathOutput+i,
                    pathOutput2+i,
                    pathOutput3+i,
                    (""+i));

            file3HBase.insert_double_values_one_row_with_index(i,resultsExperiments);
        }

        file3HBase.close();
    }
}
