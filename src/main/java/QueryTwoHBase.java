/**
 * Created by giorgio on 07/06/17.
 */
import com.google.protobuf.ServiceException;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.lang.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.*;
public class QueryTwoHBase {

    private static final String HBASE_QUERYTWO = "queryTwo";

    private static byte[] b(String s){
        return Bytes.toBytes(s);
    }

    public static class CalculatePhaseMapper extends Mapper<Object, Text, Text, DoubleWritable> {

        private Text outkey = new Text();
        private DoubleWritable outValue = new DoubleWritable();

        public void map(Object key, Text value, Context context)
                throws IOException, InterruptedException {
            String line = value.toString();
            String[] part = line.split("\t");
            if(part.length!=2){
                return;
            }
            outkey.set((part[0]).toString());
            outValue.set(Double.parseDouble(part[1]));
            context.write(outkey, outValue);
        }
    }
    public static class CalculatePhaseReducer extends
            TableReducer<Text, DoubleWritable, ImmutableBytesWritable> {

        public void reduce(Text key, Iterable<DoubleWritable> values, Context context)
                throws IOException, InterruptedException {
            double v = 0.0;
            double avg = 0.0;
            double dist = 0.0;
            double n_rep = 0.0;
            double std =0.0;
            String s = " ";
            double add = 0.0;
            for (DoubleWritable t : values) {
                add = t.get();
                n_rep = n_rep + 1;
                dist = (add - avg);
                v = v + (dist*dist)*(n_rep-1)/(n_rep);
                avg = avg + dist/n_rep;
            }
            std = Math.sqrt(v/n_rep);


            if(n_rep!=0){
                Put p = new Put(b("row"+i));

                ++i;

                p.add(b("fam1"),b("Genre"),b(key.toString()));
                p.add(b("fam1"),b("Average Rating"),b(""+avg));
                p.add(b("fam1"),b("STD. Rating"),b(""+std));

                context.write(null, p);
                //context.write(key, outValue);
            }else System.out.println(key + " ");
        }
    }

    public static abstract class GenericHierarchyMapper extends Mapper<Object, Text, Text, Text> {

        private Text outKey = new Text();
        private Text outValue = new Text();
        private final String valuePrefix;

        protected GenericHierarchyMapper(String valuePrefix) throws ParseException {
            this.valuePrefix = valuePrefix;
        }

        @Override
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

            String line = value.toString();
            if(valuePrefix.equals("R")){
                String[] parts = line.split(",");
                if (parts.length != 4)
                    return;
                if(!(parts[3]).equals("timestamp")){
                    String id = parts[1];
                    String content = parts[2];

                    outKey.set(id);
                    outValue.set(valuePrefix + content);
                    context.write(outKey, outValue);
                }
            }else{
                String[] parts = line.split("," , 2);
                if(parts.length !=2)
                    return;
                int i  = (parts[1]).lastIndexOf(",");
                if(!parts[0].equals("movieId")){
                    String id =parts[0];
                    outKey.set(id);
                    int lastComma = parts[1].lastIndexOf(",");
                    String gen = parts[1].substring(lastComma+1, parts[1].length());
                    String[] genres = gen.split("\\|");
                    for (String genre : genres ) {
                        outValue.set(valuePrefix + genre);
                        context.write(outKey,outValue);
                    }
                }
            }
        }
    }

    public static class RatingMapper extends GenericHierarchyMapper {
        public RatingMapper() throws ParseException {
            super("R");
        }
    }

    public static class MoviesMapper extends GenericHierarchyMapper {
        public MoviesMapper() throws ParseException {
            super("M");
        }
    }

    public static class TopicHierarchyReducer extends
            Reducer<Text, Text, Text, Text> {

        public enum ValueType {RATING, MOVIES, UNKNOWN}
        private Text outKey = new Text();
        private Text outValue = new Text();


        @Override
        public void reduce(Text key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {
            double res = 0.0;
            double sum = 0.0;
            double n_rep = 0.0;
            String s = " ";
            ArrayList<String> gen = new ArrayList();
            for (Text t : values) {
                String value = t.toString();
                if (ValueType.RATING.equals(discriminate(value))) {
                    s = value.substring(1,value.length());
                    double add = Double.parseDouble(s);
                    sum = sum + add;
                    n_rep = n_rep + 1;
                } else if (ValueType.MOVIES.equals(discriminate(value))){
                    gen.add(value.substring(1,value.length()));
                }
            }
            res = sum / n_rep;

            if(n_rep > 0){
                String serializedTopic = "" + res;
                outValue.set(new Text(serializedTopic));
                for (int i = 0; i< gen.size(); i++ ){
                    outKey.set(gen.get(i));
                    context.write(outKey, outValue);
                }
            }
        }

        private ValueType discriminate(String value){

            char d = value.charAt(0);
            switch (d){
                case 'R':
                    return ValueType.RATING;
                case 'M':
                    return ValueType.MOVIES;
            }

            return ValueType.UNKNOWN;
        }

        private String getContent(String value){
            return value.substring(1);
        }

    }

    private static int i = 0;

    public static double[] runTest(int numReducerFirstTask, String... args) throws IOException, ClassNotFoundException, InterruptedException, ServiceException {

        Configuration conf = HBaseConfiguration.create();

                    /* Check configuration */
        HBaseAdmin.checkHBaseAvailable(conf);

        HBaseAdmin admin = new HBaseAdmin(conf);


                    /* Init 'queryOne' HBase Table */
        if ( admin.tableExists(HBASE_QUERYTWO))
        {
            admin.disableTable(HBASE_QUERYTWO);
            admin.deleteTable(HBASE_QUERYTWO);

            HTableDescriptor tableDescriptor = new HTableDescriptor(TableName.valueOf(HBASE_QUERYTWO));
            tableDescriptor.addFamily(new HColumnDescriptor("fam1"));
            admin.createTable(tableDescriptor);
        }


            /* Set output files/directories using command line arguments */
        if ( Integer.parseInt(args[3]) == 0)
        {
            args[2] = args[2].substring(0,args[2].length()-1);
        }


        Path inputPath1 = new Path(args[0]);
        Path inputPath2 = new Path(args[1]);
        Path outputStage = new Path(args[2]);

        /* Create and configure a new MapReduce Job */
        Job job = Job.getInstance(conf, "QueryTwoHBase");
        job.setJarByClass(QueryTwoHBase.class);

            /* Set output files/directories using command line arguments */
        if ( Integer.parseInt(args[3]) == 0)
        {
            args[2] = args[2].substring(0,args[2].length()-1);
        }

        /* Map function, from multiple input file */
        MultipleInputs.addInputPath(job, inputPath1, TextInputFormat.class, RatingMapper.class);
        MultipleInputs.addInputPath(job, inputPath2, TextInputFormat.class, MoviesMapper.class);

        /* Reduce function */
        job.setReducerClass(TopicHierarchyReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setNumReduceTasks(numReducerFirstTask);

        /* Set output files/directories using command line arguments */
        FileOutputFormat.setOutputPath(job, outputStage);
        job.setOutputFormatClass(TextOutputFormat.class);

        double[] executionTime = {0.0, 0.0, 0.0};

        long start = new Date().getTime();

        /* Wait for job termination */
        int code = job.waitForCompletion(true) ? 0 : 1;

        if(code == 0) {

            long endFirstStep = new Date().getTime();

            Job avgGenres = Job.getInstance(conf,"CalculateAVG");
            avgGenres.setJarByClass(QueryTwoHBase.class);

          /*set mapper second phase*/
            avgGenres.setMapperClass(CalculatePhaseMapper.class);
            avgGenres.setMapOutputKeyClass(Text.class);
            avgGenres.setMapOutputValueClass(DoubleWritable.class);

          /*Reduce identity*/
            TableMapReduceUtil.addDependencyJars(avgGenres);
            TableMapReduceUtil.initTableReducerJob(HBASE_QUERYTWO,CalculatePhaseReducer.class,avgGenres );
            avgGenres.setReducerClass(CalculatePhaseReducer.class);
            //avgGenres.setNumReduceTasks(1);
            avgGenres.setOutputKeyClass(Text.class);
            avgGenres.setOutputValueClass(Text.class);

          /* Set input and output files: the input is the previous job's output */
            avgGenres.setInputFormatClass(TextInputFormat.class);
            TextInputFormat.setInputPaths(avgGenres, outputStage);

            long startSecondStep = new Date().getTime();

            avgGenres.waitForCompletion(true);

            long end = new Date().getTime();

            FileSystem hdfs = FileSystem.get(conf);


                    /*
                     *  Delete Multiple Data generated into a multi run testing session
                     */
            if ( Integer.parseInt(args[3]) != 0)
            {
                hdfs.delete(new Path(args[2]),true);
            }

                    /*
                     * Execution time for the all run
                     */
            executionTime[0] = (end - start) / 1000;

                    /*
                     * Execution time for the first map-reduce step
                     */
            executionTime[1] = (endFirstStep - start) / 1000;

                    /*
                     * Execution time for the second map-reduce step
                     */
            executionTime[2] = (end - startSecondStep) / 1000;
        }

        return executionTime;
    }
}
