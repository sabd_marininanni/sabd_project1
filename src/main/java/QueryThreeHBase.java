import com.google.gson.Gson;
import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import util.MiscUtils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class QueryThreeHBase {

    private static final String HBASE_QUERYTHREE = "queryThree";

    private static byte[] b(String s){
        return Bytes.toBytes(s);
    }

    public static long getTimeStamp(String dateString){
      long unixTime = 0;
      try{
          DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
          Date date = dateFormat.parse(dateString);
          unixTime = (long) date.getTime()/1000;
        }catch(ParseException e){
          e.printStackTrace();
        }
        return unixTime;
    }
    public static class TotalPositionMapper extends Mapper<Object, Text, Text, DoubleWritable> {
        private Text movieKey = new Text();
        private DoubleWritable ratingValues = new DoubleWritable();
        private long thresholdTimestampDown = getTimeStamp("01-04-2013");
        private long thresholdTimestampUpper = getTimeStamp("31-03-2014");

        @Override
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

            String line = value.toString();
            String[] parts = line.split("," , 4);
            if(parts.length !=4){
              return;
            }
            if(!(parts[1]).equals("movieId")){
              long timestamp = Long.parseLong(parts[3]);
              if((timestamp<thresholdTimestampUpper) && (timestamp>=thresholdTimestampDown)){
                movieKey.set(parts[1]);
                double rating = Double.parseDouble(parts[2]);
                ratingValues.set(rating);
                context.write(movieKey,ratingValues);
              }
            }
        }
    }

    public static class TotalPositionReducer extends Reducer<Text, DoubleWritable, Text, Text> {

      private Map<Text , DoubleWritable> countMap = new HashMap<Text,DoubleWritable>();

      private DoubleWritable avgR = new DoubleWritable();

        @Override
        public void reduce(Text key, Iterable<DoubleWritable> values , Context context) throws IOException, InterruptedException {
            double avgRating = 0.0;
            double sum = 0.0;
            double n_rep = 0.0;

            for(DoubleWritable value : values){
              sum = sum + value.get();
              n_rep = n_rep + 1;
            }

            if(n_rep >= 10){
              avgRating = sum /n_rep;

              countMap.put(new Text(key), new DoubleWritable(avgRating));
            }
        }
        @Override
        protected void cleanup(Context context) throws IOException,InterruptedException{
          Map<Text,DoubleWritable> sortedMap = MiscUtils.sortByValues(countMap);
          int counter = 0;
          int posizione = 1;

          for(Text key: sortedMap.keySet()){
            String serializedPosition = (posizione + "," + sortedMap.get(key));
            context.write(key,new Text(serializedPosition));
            posizione ++;
          }
        }
    }

    public static class TopTenMapper extends Mapper<Object, Text, Text, DoubleWritable> {
        private Text movieKey = new Text();
        private DoubleWritable ratingValues = new DoubleWritable();
        private long thresholdTimestampDown = getTimeStamp("01-04-2014");
        private long thresholdTimestampUpper = getTimeStamp("31-03-2015");



        @Override
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

            String line = value.toString();
            String[] parts = line.split("," , 4);
            if(parts.length !=4){
              return;
            }
            if(!(parts[1]).equals("movieId")){
              long timestamp = Long.parseLong(parts[3]);
              if((timestamp<thresholdTimestampUpper) && (timestamp>=thresholdTimestampDown)){
                movieKey.set(parts[1]);
                double rating = Double.parseDouble(parts[2]);
                ratingValues.set(rating);
                context.write(movieKey,ratingValues);
              }
            }
        }
    }

    public static class TopTenReducer extends Reducer<Text, DoubleWritable, Text, Text> {

      private Map<Text , DoubleWritable> countMap = new HashMap<Text,DoubleWritable>();

      private Gson gson = new Gson();
      private DoubleWritable avgR = new DoubleWritable();

        @Override
        public void reduce(Text key, Iterable<DoubleWritable> values , Context context) throws IOException, InterruptedException {
            double avgRating = 0.0;
            double sum = 0.0;
            double n_rep = 0.0;

            for(DoubleWritable value : values){
              sum = sum + value.get();
              n_rep = n_rep + 1;
            }

            if(n_rep >= 10 ){
              avgRating = sum /n_rep;

              countMap.put(new Text(key), new DoubleWritable(avgRating));
            }
        }
        @Override
        protected void cleanup(Context context) throws IOException,InterruptedException{
          Map<Text,DoubleWritable> sortedMap = MiscUtils.sortByValues(countMap);
          int counter = 0;
          int posizione = 1;

          for(Text key: sortedMap.keySet()){
            counter = counter + 1;
            if(counter == 11){
              break;
            }

            String serializedPosition = (posizione + "," + sortedMap.get(key));
            context.write(key,new Text(serializedPosition));
            posizione ++;
          }
        }
    }

    public static class deltaMapper extends Mapper<Object,Text,Text,Text>{
      private Text outKey = new Text();
      private Text outValue = new Text();
      private final String valuePrefix;

      protected deltaMapper(String valuePrefix)throws ParseException{
        this.valuePrefix = valuePrefix;
      }

      public void map(Object Key, Text value, Context context) throws IOException,InterruptedException{
        String line = value.toString();
        String[] part = line.split("\t");
        if(part.length!=2){
          return;
        }
        String[] secPiece = part[1].split(",",2);
        String key = part[0];
        outKey.set(key);
        if(valuePrefix.equals("T")){
          outValue.set(valuePrefix + secPiece[0]);
          context.write(outKey,outValue);
        }else if (valuePrefix.equals("K")){
          outValue.set(valuePrefix + secPiece[0]);
          context.write(outKey,outValue);

        }
      }
    }

    public static class TopTen extends deltaMapper{
      public TopTen() throws ParseException{
        super("T");
      }
    }

    public static class TotClas extends deltaMapper{
      public TotClas() throws ParseException{
        super("K");
      }
    }


    public static class deltaReduce extends Reducer<Text,Text,Text,Text>{
      private Map<Text , Text> countMap = new HashMap<Text,Text>();
      public enum ValueType {TOPTEN,TOPK,UNKNOWN}



    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException{

      int lastYear = 0;
      int compare = 0;
      int delta = 0;
      String s = " ";
      int position = 0;
      for(Text t: values){
        String value = t.toString();
        if(ValueType.TOPTEN.equals(discriminate(value))){
          s = value.substring(1,value.length());
          position = Integer.parseInt(s);
          lastYear = position;
        }else if(ValueType.TOPK.equals(discriminate(value))){
          s = value.substring(1,value.length());
          position = Integer.parseInt(s);
          compare = position;
        }
      }
      if(lastYear!=0){
        if(compare!=0){
          delta = compare - lastYear;
          String res = (lastYear + "," + compare + "," + delta);
          countMap.put(new Text(key), new Text(res));
        }else{
          delta = compare;
          String res = (lastYear + "," + compare + "," + delta);
          countMap.put(new Text(key), new Text(res));
        }
      }
    }
    @Override
    protected void cleanup(Context context) throws IOException,InterruptedException{
      Map<Text,Text> sortedMap = MiscUtils.sortByValues(countMap);
      int count = 0;
      for(Text key: sortedMap.keySet()){
        if(count == 10){
          break;
        }
        //String serializedPosition = (sortedMap.get(key));
        context.write(key,new Text(sortedMap.get(key)));
      }
    }

    private ValueType discriminate(String value){
      char d = value.charAt(0);
      switch (d){
          case 'T':
              return ValueType.TOPTEN;
          case 'K':
              return ValueType.TOPK;
      }

      return ValueType.UNKNOWN;
    }

    private String getContent(String value){
        return value.substring(1);
    }
  }

  public static class JoinMapper extends Mapper<Object,Text,Text,Text>{
    private Text outKey = new Text();
    private Text outValue = new Text();
    private final String valuePrefix;

    protected JoinMapper(String valuePrefix)throws ParseException{
      this.valuePrefix = valuePrefix;
    }

    public void map(Object Key, Text value, Context context) throws IOException,InterruptedException{
      String line = value.toString();
      if(valuePrefix.equals("D")){
        String[] part = line.split("\t");
        if(part.length!=2){
          return;
        }
        String key = part[0];
        outKey.set(key);
        outValue.set(valuePrefix + part[1]);
        context.write(outKey,outValue);
      }else if (valuePrefix.equals("M")){
        String[] part = line.split("," , 2);
        if(part.length!=2){
          return;
        }
        if(!part[0].equals("movieId")){
          String key = part[0];
          int lastComma = part[1].lastIndexOf(",");
          String gen = part[1].substring(0, lastComma);
          outKey.set(key);
          outValue.set(valuePrefix + gen);
          context.write(outKey,outValue);
        }
      }
    }
  }

  public static class Delta extends JoinMapper{
    public Delta() throws ParseException{
      super("D");
    }
  }

  public static class Movies extends JoinMapper{
    public Movies() throws ParseException{
      super("M");
    }
  }


  public static class JoinReduce extends
          TableReducer<Text, Text, ImmutableBytesWritable>
  {
    private Map<Text , Text> countMap = new HashMap<Text,Text>();
    public enum ValueType {DELTA,MOVIES,UNKNOWN}



  @Override
  public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException{
    String film = "";
    String res = "";
  for(Text t: values){
      String value = t.toString();
      if(ValueType.DELTA.equals(discriminate(value))){
        res = value.substring(1,value.length());
      }else if(ValueType.MOVIES.equals(discriminate(value))){
        film = value.substring(1,value.length());

      }
    }
    if(!res.equals(""))
    {
        String[] ratings = res.split(",");


        Put p = new Put(b("row"+i));

        ++i;

        p.add(b("fam1"),b("Film"),b(""+film));
        p.add(b("fam1"),b("Rating 2014-15"),b(""+ratings[0]));
        p.add(b("fam1"),b("Rating 2013-14"),b(""+ratings[1]));
        p.add(b("fam1"),b("Var. Rating"),b(""+ratings[2]));

        context.write(null, p);
    }
  }

  private ValueType discriminate(String value){
    char d = value.charAt(0);
    switch (d){
        case 'D':
            return ValueType.DELTA;
        case 'M':
            return ValueType.MOVIES;
    }

    return ValueType.UNKNOWN;
  }

}


    private static int i = 0;


    public static double[] runTest(int numReducerFirstTask, int numReducerSecondTask, String... args) throws IOException, ClassNotFoundException, InterruptedException, ServiceException {

        Configuration conf = HBaseConfiguration.create();

                    /* Check configuration */
        HBaseAdmin.checkHBaseAvailable(conf);

        HBaseAdmin admin = new HBaseAdmin(conf);


                    /* Init 'queryOne' HBase Table */
        if ( admin.tableExists(HBASE_QUERYTHREE))
        {
            admin.disableTable(HBASE_QUERYTHREE);
            admin.deleteTable(HBASE_QUERYTHREE);

            HTableDescriptor tableDescriptor = new HTableDescriptor(TableName.valueOf(HBASE_QUERYTHREE));
            tableDescriptor.addFamily(new HColumnDescriptor("fam1"));
            admin.createTable(tableDescriptor);
        }

                    /* Set output files/directories using command line arguments */
        if ( Integer.parseInt(args[5]) == 0)
        {
            args[2] = args[2].substring(0,args[2].length()-1);
            args[3] = args[3].substring(0,args[3].length()-1);
            args[4] = args[4].substring(0,args[4].length()-1);
        }

        /* Create and configure a new MapReduce Job */
        Path inputPath1 = new Path(args[0]);
        Path movies = new Path(args[1]);
        Path TopTen = new Path(args[2]);
        Path TotalClassifier = new Path(args[3]);
        Path FinalResult = new Path(args[4]);


        Job job = Job.getInstance(conf, "TopTen");
        job.setJarByClass(QueryThreeHBase.class);

        /* Map function */
        job.setMapperClass(TopTenMapper.class);
        // if equal to the reduce output, can be omitted
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(DoubleWritable.class);

        /* Combine function */

        /* Reduce function */
        job.setNumReduceTasks(numReducerFirstTask);
        job.setReducerClass(TopTenReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        /* Set input and output files/directories using command line arguments */
        FileInputFormat.addInputPath(job, inputPath1);
        FileOutputFormat.setOutputPath(job, TopTen);
        // if these files are different from text files, we can specify the format
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        double[] executionTime = {0.0,0.0,0.0,0.0,0.0};

        long start = new Date().getTime();

        int code = job.waitForCompletion(true) ? 0 : 1;

        if(code == 0){

            long endFirstStep = new Date().getTime();

          Job TotalOrder = Job.getInstance(conf,"Total Position");
          TotalOrder.setJarByClass(QueryThreeHBase.class);

          /*set mapper and setter*/
          TotalOrder.setMapperClass(TotalPositionMapper.class);
          TotalOrder.setMapOutputKeyClass(Text.class);
          TotalOrder.setMapOutputValueClass(DoubleWritable.class);


          /*Reduce*/
          TotalOrder.setReducerClass(TotalPositionReducer.class);
          TotalOrder.setNumReduceTasks(numReducerSecondTask);
          TotalOrder.setOutputKeyClass(Text.class);
          TotalOrder.setOutputValueClass(Text.class);

          /*input file */
          TotalOrder.setInputFormatClass(TextInputFormat.class);
          TextInputFormat.setInputPaths(TotalOrder,inputPath1);
          TextOutputFormat.setOutputPath(TotalOrder, TotalClassifier);

          long startSecondStep = new Date().getTime();

          code = TotalOrder.waitForCompletion(true) ? 0 : 2 ;

          if(code ==0){

              long stopSecondStep = new Date().getTime();

            Job delta = Job.getInstance(conf, "DeltaClass");
            delta.setJarByClass(QueryThreeHBase.class);


            /* Map function, from multiple input file */
            MultipleInputs.addInputPath(delta, TopTen, TextInputFormat.class, TopTen.class);
            MultipleInputs.addInputPath(delta, TotalClassifier, TextInputFormat.class, TotClas.class);

            /* Reduce function */
            delta.setReducerClass(deltaReduce.class);
            delta.setOutputKeyClass(Text.class);
            delta.setOutputValueClass(Text.class);

            /* Set output files/directories using command line arguments */
            FileOutputFormat.setOutputPath(delta, FinalResult);
            delta.setOutputFormatClass(TextOutputFormat.class);

            /* Wait for job termination */

            long startThirdStep = new Date().getTime();

            code = delta.waitForCompletion(true)? 0 : 3;

            if(code == 0){

                long stopThirdStep = new Date().getTime();

              Job joinName = Job.getInstance(conf,"Join class");
              joinName.setJarByClass(QueryThreeHBase.class);

              /*Map function form multiple input*/
              TableMapReduceUtil.addDependencyJars(joinName);
              TableMapReduceUtil.initTableReducerJob(HBASE_QUERYTHREE, JoinReduce.class, joinName );

              MultipleInputs.addInputPath(joinName,FinalResult,TextInputFormat.class, Delta.class);
              MultipleInputs.addInputPath(joinName,movies,TextInputFormat.class, Movies.class);

              /*Reduce function */
              joinName.setReducerClass(JoinReduce.class);
              joinName.setOutputKeyClass(Text.class);
              joinName.setOutputValueClass(Text.class);

              /*Set outputFile*/

              long startFouthStep = new Date().getTime();

              code = joinName.waitForCompletion(true) ? 0 : 1;

              long end = new Date().getTime();


                FileSystem hdfs = FileSystem.get(conf);

                    /*
                     *  Delete Multiple Data generated into a multi run testing session
                     */
                if ( Integer.parseInt(args[5]) != 0)
                {
                    hdfs.delete(new Path(args[2]),true);
                    hdfs.delete(new Path(args[3]),true);
                    hdfs.delete(new Path(args[4]),true);
                }

                    /*
                     *  Calc Execution Time single step.
                     */
              executionTime[0] = (end-start)/1000;

                    /*
                     * Execution time for the first map-reduce step
                     */
                executionTime[1] = (endFirstStep-start)/1000;

                    /*
                     * Execution time for the second map-reduce step
                     */
                executionTime[2] = (stopSecondStep-startSecondStep)/1000;

                    /*
                     * Execution time for the third map-reduce step
                     */
                executionTime[3] = (stopThirdStep-startThirdStep)/1000;

                    /*
                     * Execution time for the fourth map-reduce step
                     */
                executionTime[4] = (end-startFouthStep)/1000;
            }
          }
        }

        return executionTime;
    }
}
