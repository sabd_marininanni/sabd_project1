
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * CSV File API
 *
 * @author  Davide Nanni
 * @version %I%, %G%
 *
 */

public class CSVFileBasic
{
            /**
             *  Default Delimiter and Separator
             */
    protected static final String COMMA_DELIMITER = ",";
    protected static final String NEW_LINE_SEPARATOR = "\n";

            /**
             *  Results Files Path
             */
    protected String name="";

    protected FileWriter csvFile = null;



    public static String getCommaDelimiter()
    {
        return COMMA_DELIMITER;
    }


    public static String getNewLineSeparator()
    {
        return NEW_LINE_SEPARATOR;
    }
            /*
             * Write only one string as header
             */
    public void insert_single_header(String FILE_HEADER)
    {
        try
        {
            csvFile.append(FILE_HEADER);
        }
        catch(Exception e)
        {
            System.out.println("[ERROR] Failed write the header!\n");
            e.printStackTrace();
            System.exit(1);
        }
    }

            /*
             * Write a set of strings as header
             */
    public void insert_header(String[] header)
    {
        int i = 0;
        for( ; i<(header.length-1); ++i )
        {
            insert_single_header(header[i]);
            insert_comma_delimiter();
        }

        insert_single_header(header[header.length-1]);

        insert_new_line_separator();
    }


            /**
             * Insert the default comma delimiter between values or column header
             */
    public void insert_comma_delimiter()
    {
        try
        {
            csvFile.append(COMMA_DELIMITER);
        }
        catch(Exception e)
        {
            System.out.println("[ERROR] Failed write the comma delimiter!\n");
            e.printStackTrace();
            System.exit(1);
        }
    }

            /**
             * Insert the default new line separator to write values into new line
             */
    public void insert_new_line_separator()
    {
        try
        {
            csvFile.append(NEW_LINE_SEPARATOR);
        }
        catch(Exception e)
        {
            System.out.println("[ERROR] Failed write the new line separator!\n");
            e.printStackTrace();
            System.exit(1);
        }
    }



    /**
     * Insert a String value
     */
    public void insert_value(String value)
    {
        try
        {
            csvFile.append(""+value);
        }
        catch(Exception e)
        {
            System.out.println("[ERROR] Failed write the value!\n");
            e.printStackTrace();
            System.exit(1);
        }
    }

            /**
             * Insert a Double value
             */
    public void insert_value(double value)
    {
        try
        {
            csvFile.append(""+value);
        }
        catch(Exception e)
        {
            System.out.println("[ERROR] Failed write the value!\n");
            e.printStackTrace();
            System.exit(1);
        }
    }


            /**
             * Insert the double values set into one column
             */
    public void insert_double_values_one_column(double[] values)
    {
        for(double v : values)
        {
            insert_value(v);
            insert_new_line_separator();
        }
    }


            /**
             * Insert the double values set into one row
             */
    public void insert_double_values_one_row( double[] values )
    {
        int i = 0,
            indexLastValues = values.length-1;

        for(double v: values)
        {
            insert_value(v);

            if ( i < indexLastValues )
                insert_comma_delimiter();
            else
                insert_new_line_separator();

            ++i;
        }
    }

    /**
     * Insert the double values set into one row after a integer value (the index)
     */
    public void insert_double_values_one_row_with_index( int index, double[] values )
    {
        int i = 0,
                indexLastValues = values.length-1;

        for(double v: values)
        {
            insert_value(index);
            insert_comma_delimiter();

            insert_value(v);

            if ( i < indexLastValues )
                insert_comma_delimiter();
            else
                insert_new_line_separator();

            ++i;
        }
    }

            /*
             * Close the current csv file
             */
    public void close()
    {
        try
        {
            csvFile.flush();
            csvFile.close();
        }
        catch(IOException e)
        {
            System.out.println("[ERROR] Failed flush/close csv file\n");
            e.printStackTrace();
            System.exit(1);
        }
    }


            /*
             * Constructor
             */
    public CSVFileBasic(String name, String path_dir) throws IOException
    {
        this.name = name;

        csvFile = new FileWriter(path_dir+"/"+this.name+".csv");
    }

           /*
            * Constructor with header
            */
    public CSVFileBasic(String name, String path_dir, String[] header) throws IOException
    {
        this.name = name;

        csvFile = new FileWriter(path_dir+"/"+this.name+".csv");

        insert_header(header);
    }
}
