#!/bin/bash
#set your hadoop path
HADOOP_PATH="/usr/local/hadoop/sbin"
#set your hbase path
HBASE_PATH="/usr/local/Hbase/bin"

#start HDFS
"$HADOOP_PATH"/start-all.sh

#start HBase
sudo "$HBASE_PATH"/start-hbase.sh

#cd /directory where you have the jar
cd /home/giorgio

#This Jar creates the Hbase tables for each Queries
hadoop jar hbase-client.jar HbaseClient

#This Jar launch the queries MapReduce
hadoop jar SabdMapReduce.jar SABD_Project

sudo "$HBASE_PATH"/stop-hbase.sh
"$HADOOP_PATH"/stop-all.sh

