import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.IOException;

public class HBaseClient {

    /* Configuration Parameters */
    private static final String ZOOKEEPER_HOST = "localhost";
    private static final String ZOOKEEPER_PORT = "2181";
    private static final String HBASE_MASTER  = "localhost:60000";

    private static final boolean DEBUG = true;

    private enum ALTER_COLUMN_FAMILY {
        ADD, DELETE
    }

    private Connection connection = null;

    private static byte[] b(String s){
        return Bytes.toBytes(s);
    }

    /**
     * Create a connection with HBase
     * @return
     * @throws IOException
     * @throws ServiceException
     */
    public Connection getConnection() throws IOException, ServiceException {

        if (!(connection == null || connection.isClosed() || connection.isAborted()))
            return connection;

        if (!DEBUG || true)
            Logger.getRootLogger().setLevel(Level.ERROR);

        Configuration conf  = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", ZOOKEEPER_HOST);
        conf.set("hbase.zookeeper.property.clientPort", ZOOKEEPER_PORT);
        conf.set("hbase.master", HBASE_MASTER);

        /* Check configuration */
        HBaseAdmin.checkHBaseAvailable(conf);

        if (DEBUG)
            System.out.println(" HBase is running!");

        this.connection = ConnectionFactory.createConnection(conf);
        return connection;
    }

    /* *******************************************************************************
    *  Database administration
    * ******************************************************************************* */

    /**
     * Create a new table named tableName, with the specified columnFamilies
     *
     * @param tableName
     * @param columnFamilies
     * @return
     */
    public boolean createTable(String tableName, String... columnFamilies) {

        try {

            Admin admin = getConnection().getAdmin();
            HTableDescriptor tableDescriptor = new HTableDescriptor(TableName.valueOf(tableName));

            for (String columnFamily : columnFamilies) {
                tableDescriptor.addFamily(new HColumnDescriptor(columnFamily));
            }

            admin.createTable(tableDescriptor);
            return true;

        } catch (IOException | ServiceException e) {
            e.printStackTrace();
        }

        return false;

    }



    public void close() throws IOException
    {
        connection.close();
    }
    /**
     * Check if a table exists
     * @param table table name
     * @return  true if a table exists
     */
    public boolean exists(String table){

        try {

            Admin admin = getConnection().getAdmin();
            TableName tableName = TableName.valueOf(table);
            return admin.tableExists(tableName);

        } catch (IOException | ServiceException e) {
            e.printStackTrace();
        }

        return false;

    }


    public void createTablesQuery(String... tables) throws IOException, ServiceException {

         /* **********************************************************
         *  Table Management: Create, Alter, Describe, Delete
         * ********************************************************** */


        //  Create
        System.out.println("\n ******************************************************** \n");

        for(String tab:tables)
        {
            if (!exists(tab)){
                System.out.println("\n Creating table...");
                createTable(tab, "fam1");
                System.out.println(" [TABLE CREATED] -- "+tab);
            }
            else
            {
                System.out.println("\n [ERROR] -- "+tab+" is already into HBase! ");
                System.out.println(" [ERROR] ----  Delete this one before run the App!");
                System.out.println("\n ---------------------------------------------------------------------- ");
            }
        }

        System.out.println("\n ******************************************************** \n");
    }


    public static void main(String args[]) throws IOException, ServiceException
    {
        HBaseClient h1 = new HBaseClient();

        h1.createTablesQuery("queryOne","queryTwo","queryThree");

        h1.close();
    }

}
