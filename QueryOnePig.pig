data_rating = LOAD 'hdfs:///ratingPUT' USING PigStorage(',') AS (userId:long, movieId:long, rating:double, timestamp:long);
data_movies = LOAD 'hdfs:///moviesPUT' USING PigStorage(',') AS (movieId:long, title:chararray, genres:chararray);
data_rating_filte = filter data_rating by ToDate(timestamp)>=ToDate('2000/01/01');
datafinal = join data_rating by movieId, data_movies by movieId;
grpd = group datafinal by data_rating::movieId;
cnt = foreach grpd generate datafinal.data_movies::title, AVG(datafinal.data_rating::rating);
STORE cnt INTO 'hdfs:///QueryOnePIG';




